export let varlidator = {
  kiemTraRong: (valueInput, idError) => {
    if (valueInput == "") {
      document.getElementById(idError).style.display = "block";
      document.getElementById(idError).innerText =
        "Trường này không được để trống";
      return false;
    } else {
      document.getElementById(idError).style.display = "none";
      return true;
    }
  },
  kiemTraTenSanPhamTrungLap: (valueInput, productList) => {
    var index = productList.findIndex((product) => {
      return product.name.toLowerCase() == valueInput.toLowerCase();
    });

    if (index !== -1) {
      document.getElementById("tbTen").style.display = "block";
      document.getElementById("tbTen").innerText =
        "Tên sản phẩm không được trùng lặp";
      return false;
    } else {
      document.getElementById("tbTen").style.display = "none";
      return true;
    }
  },
};
